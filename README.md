# Hubber

![Hubber](https://i.ibb.co/3M4k0kc/Hubber.png)

### * This project is still under maintenance * ###

[![Google maps](https://i.imgur.com/5NDGLJY.png)](https://www.google.com/maps)[![SpringBoot](https://i.imgur.com/CavgUMj.png)](https://spring.io/projects/spring-boot)

Hubber is a location based app, offline-disable.
Hubber is writen in:
  - Java
  - Gherkin (tests)
---
## Upcoming Features!

  - Search for the nearest hub
  - Suggest an hub
  - Watch hub information
  - Live pin point location on Google map showing you where are you relative to the choosen hub
---
### Prerequisites
Please make sure you have the following services on your machine:
[Java JDK V1.8](https://www.oracle.com/java/technologies/javase-jdk8-downloads.html)
[Apache Maven V3.6.3](https://maven.apache.org/)
[Soring boot V2.2.5.RELEASE](https://mvnrepository.com/artifact/org.springframework.boot/spring-boot/2.2.5.RELEASE)
[JUnit V5.5.2](https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api/5.5.2)
---
## Getting Started

So you want to start play with the code, you got it!
Clone the repository to your machine,
Open CMD, rediract to the project folder and run:
```
mvn clean verify
```
it will add a `./target` folder inside is the jar ready to use,
Open CMD angin and rediract to the new target folder and run:
```
java -jar hubber_XXX.jar
```
And the server is up and running!

---
## Server URLs
You can reach the following URLs:

| URL                                          | Method | Accepts  | Return |       `{Variable}`      |         Description          |
| :------------------------------------------- |:------:| :-------:| :----: | :---------------------: | :--------------------------: |
| /acs/users/login/`{userEmail}`               | GET    |    -     |  JSON  |  Any valid user email   | Return user account details  |
| /acs/users/                                  | POST   |   JSON   |  JSON  |        -                |       Create new user        |
| /acs/users/`{userEmail}`                     | PUT    |   JSON   |    -   |  Any valid user email   |   Update an existing user    |
| /acs/elements/`{userEmail}`                  | GET    |    -     |  JSON  |  Any valid user email   |     Return all elements      |
| /acs/elements/`{userEmail}`/`{elementID}`    | GET    |    -     |  JSON  | User email, Element ID  |   Return a specific element  |
| /acs/elements/`{managerEmail}`               | POST   |   JSON   |  JSON  |     Manager email       |      Creates an element      |
| /acs/elements/`{managerEmail}`/`{elementId}` | PUT    |   JSON   |    -   |Manager email, Element ID|  Update an existing element  |
| /acs/admin/users/`{managerEmail}`            | GET    |    -     |  JSON  |     Manager email       |      Return all users        |
| /acs/admin/actions/`{managerEmail}`          | GET    |    -     |  JSON  |     Manager email       |      Return all actions      |
| /acs/admin/users/`{managerEmail}`            | DELETE |    -     |    -   |     Manager email       |      Delete all users        |
| /acs/admin/elements/`{managerEmail}`         | DELETE |    -     |    -   |     Manager email       |      Delete all elements     |
| /acs/admin/actions/`{managerEmail}`          | DELETE |    -     |    -   |     Manager email       |      Delete all actions      |


---

## Running the tests

Rediract to `src/test/java/com/hubber` you can find a test class for every controller in our project.
Also when running 
```
mvn clean verify
```
Maven will check if the tests in passing.

---

## Versioning

Every version goes through a [Bitbucket Pipeline](https://bitbucket.org/product/features/pipelines) 
##### Test -> Build -> Deploy 

Version number:
    - We use [Git Tags](https://git-scm.com/book/en/v2/Git-Basics-Tagging) for versioning. 
    - For the versions available, see the tags on this repository.

---

## Authors

* **Valentine Yamanov** - *Team Leader* - [Bitbucker Profile](https://bitbucket.org/%7B942c0dae-161f-4b71-818b-7946ecdcb136%7D/)
* **Meshi Sanker** - *DBA/Frontend* - [Bitbucker Profile](https://bitbucket.org/%7B34424ad4-79f5-42c2-b73c-0da283d0a5d7%7D/)
* **Omri Gilhar** - *DevOps/Backend* - [Bitbucker Profile](https://bitbucket.org/%7Ba48d748a-6226-42b5-99b4-01b7077cafd6%7D/)
* **Eyal Reis** - *Backend/Coder* - [Bitbucker Profile](https://bitbucket.org/%7Be96e8fba-1e9e-44bf-bd34-66ac1089d7a1%7D/)

## License

This project is private.
