package acs.data;

import java.util.Date;

//JPA
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="USERS")
public class UserEntity {
	private String email; // email as ID
	private UserRole role;
	private String username;
	private String avatar;
	private Date creation;
	private Location location; // latitude and longitude of the User
	
	
	
	public UserEntity() {
		this.location = new Location();
		
	}
	
	@Id
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Enumerated(EnumType.STRING)
	public UserRole getRole() {
		return role;
	}
	
	public void setRole(UserRole role) {
		this.role = role;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getAvatar() {
		return avatar;
	}
	
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreation() {
		return creation;
	}
	
	public void setCreation(Date creation) {

		this.creation = creation;
	}

	@Embedded
	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "UserEntity [email=" + email + ", role=" + role + ", username=" + username + ", avatar=" + avatar
				+ ", creation=" + creation + ", location=" + location + "]";
	}

}
