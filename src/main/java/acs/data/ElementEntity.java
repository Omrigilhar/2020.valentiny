package acs.data;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

//JPA
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="ELEMENTS")
public class ElementEntity {
	private Long elementID;
	private String type;
	private String name;
	private boolean active;
	private Date creation;
	private String createdBy;
	private Location location; // latitude and longitude of the User
	private String moreAttributes;
	private Set<ElementEntity> child;
	private ElementEntity origin;
	
	public ElementEntity() {
		this.child = new HashSet<>();
	}

	@Id
	public Long getElementID() {
		return elementID;
	}

	public void setElementID(Long elementID) {
		this.elementID = elementID;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreation() {
		return creation;
	}

	public void setCreation(Date creation) {
		this.creation = creation;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Lob
	public String getElementAttributes() {
		return moreAttributes;
	}

	public void setElementAttributes(String elementAttributes) {
		this.moreAttributes = elementAttributes;
	}

	@Embedded
	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	public ElementEntity getOrigin() {
		return origin;
	}
	
	public void setOrigin(ElementEntity origin) {
		this.origin = origin;
	}
	
	@OneToMany(mappedBy = "origin", fetch = FetchType.LAZY)
	public Set<ElementEntity> getChilds() {
		return child;
	}
	
	public void setChilds(Set<ElementEntity> child) {
		this.child = child;
	}
	
	public void addChild(ElementEntity child) {
		this.child.add(child);
		child.setOrigin(this);
	}

	@Override
	public String toString() {
		return "ElementEntity [elementID=" + elementID + ", type=" + type + ", name=" + name + ", active=" + active
				+ ", creation=" + creation + ", createdBy=" + createdBy + ", location=" + location + ", moreAttributes="
				+ moreAttributes + ", child=" + child + ", origin=" + origin + "]";
	}
}
