package acs.data;

import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.databind.ObjectMapper;

import acs.boundary.ActionBoundary;
import acs.boundary.ElementBoundary;
import acs.boundary.LocationBoundary;
import acs.boundary.UserBoundary;
import acs.boundary.UserRoleEnum;

@Component
public class EntityConverter {	
	private ObjectMapper jackson;
	
	
	@PostConstruct
	public void setup() {
		this.jackson = new ObjectMapper();
	}
	
	//User Methods
	public UserBoundary convertFromEntity(UserEntity entity) {	
		UserBoundary boundary = new UserBoundary();
		
		boundary.setEmail(entity.getEmail());
		boundary.setUsername(entity.getUsername());
		boundary.setAvatar(entity.getAvatar());
		boundary.setRole(this.fromUserEntityRole(entity.getRole()));
		
		if(entity.getLocation() != null) {
			boundary.setLocation(new LocationBoundary(entity.getLocation().getLatitude(), entity.getLocation().getLongitude()));
		}
		else{
			boundary.setLocation(null);
		}

		return boundary;		
	}
	
	public UserEntity toEntity(UserBoundary boundary) {
		UserEntity entity = new UserEntity();
		
		entity.setEmail(boundary.getEmail());
		entity.setAvatar(boundary.getAvatar());
		entity.setUsername(boundary.getUsername());
		entity.setRole(this.fromUserBounderyRole(boundary.getRole()));
					
		if(boundary.getLocation() != null) {
			entity.setLocation(new Location(boundary.getLocation().getLatitude(), boundary.getLocation().getLongitude()));		
		}
			
		return entity;	
	}
	
	//Action Methods
	public ActionBoundary convertFromEntity(ActionEntity entity) {	
		ActionBoundary boundary = new ActionBoundary();
		
		boundary.setActionId(this.fromEntityId(entity.getId()));
		boundary.setType(entity.getType());
		boundary.setCreation(entity.getCreation());
			
		try {
			boundary.setElement(this.jackson.readValue(entity.getElement(), Map.class));
			boundary.setInvokedBy(this.jackson.readValue(entity.getInvokedBy(), Map.class));
			boundary.setActionAttributes(this.jackson.readValue(entity.getMoreAttributes(), Map.class));
		} catch(Exception e) {
			throw new ConvertException("Jackson try to convert from Map to String and faild " + e);
		}
				
		return boundary;		
	}
	
	public ActionEntity toEntity(ActionBoundary boundary) {
		ActionEntity entity = new ActionEntity();
		
		entity.setId(this.toEntityId(boundary.getActionId()));
		entity.setType(boundary.getType());
		entity.setCreation(entity.getCreation());
		
		try {
			entity.setElement(this.jackson.writeValueAsString(boundary.getElement()));
			entity.setInvokedBy(this.jackson.writeValueAsString(boundary.getInvokedBy()));
			entity.setMoreAttributes(this.jackson.writeValueAsString(boundary.getActionAttributes()));
		} catch (Exception e) {
			throw new ConvertException("Jackson try to convert from String to Map and faild " + e);
		}
					
		return entity;	
	}
	
	//Element Methods
	
	public ElementBoundary convertFromEntity(ElementEntity entity) {	
		ElementBoundary boundary = new ElementBoundary();
		
		//TODO: Maybe change to Long
		boundary.setElementID(this.fromEntityId(entity.getElementID()));
		boundary.setType(entity.getType());
		boundary.setName(entity.getName());
		boundary.setActive(entity.getActive());
		boundary.setCreation(entity.getCreation());
		
		if(entity.getLocation() != null) {
			boundary.setLocation(new LocationBoundary(entity.getLocation().getLatitude(), entity.getLocation().getLongitude()));
		}
		else{
			boundary.setLocation(null);
		}
		
		try {
			boundary.setCreatedBy(this.jackson.readValue(entity.getCreatedBy(), Map.class));	
			boundary.setMoreAttributes(this.jackson.readValue(entity.getElementAttributes(), Map.class));
		} catch(Exception e) {
			throw new ConvertException("Jackson try to convert from Map to String and faild " + e);
		}
				
		return boundary;		
	}
	
	public ElementEntity toEntity(ElementBoundary boundary) {
		ElementEntity entity = new ElementEntity();
		
		if(boundary.getElementID() != null) {
			entity.setElementID(this.toEntityId(boundary.getElementID()));
		}else {
			entity.setElementID(null);
		}
		entity.setType(boundary.getType());
		entity.setName(boundary.getName());
		
		if(boundary.isActive() != null) {
			entity.setActive(boundary.isActive());
		}
		
		if(boundary.getLocation() != null) {
			entity.setLocation(new Location(boundary.getLocation().getLatitude(), boundary.getLocation().getLongitude()));
		}
		
		entity.setCreation(boundary.getCreation());
			
		try {
			entity.setCreatedBy(this.jackson.writeValueAsString(boundary.getCreatedBy()));			
			entity.setElementAttributes(this.jackson.writeValueAsString(boundary.getMoreAttributes()));

		} catch (Exception e) {
			throw new ConvertException("Jackson try to convert from String to Map and faild " + e);
		}
				
		return entity;	
	}
	
	
	//Methods
	
	public UserRoleEnum fromUserEntityRole(UserRole role) {
		
		switch(role)
		{
			case PLAYER:{	
				return UserRoleEnum.PLAYER;
			}
			case MANAGER:{	
				return UserRoleEnum.MANAGER;
			}
			case ADMIN:{	
				return UserRoleEnum.ADMIN;
			}
		}
		return null;
	}
	
	public UserRole fromUserBounderyRole(UserRoleEnum role) {
		
		switch(role)
		{
			case PLAYER:{	
				return UserRole.PLAYER;
			}
			case MANAGER:{	
				return UserRole.MANAGER;
			}
			case ADMIN:{	
				return UserRole.ADMIN;
			}
		}
		return null;
	}
	
	public Long toEntityId(String id) {
		if (id != null) {
			return Long.parseLong(id);
		}else {
			return null;
		}
	}

	public String fromEntityId(Long id) {
		if (id != null) {
			return id.toString();
		}else {
			return null;
		}
	}
	
	
	
}

