package acs.data;

import java.util.Date;

//JPA
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ACTIONS")
public class ActionEntity {
	private Long actionId;
	private String type;
	private String element; //Contains elementId
	private Date creation;
	private String invokedBy;
	private String moreAttributes; //We will add relevant attributes as needed
	
	
	public ActionEntity() {
		
	}

	@Id
	public Long getId() {
		return actionId;
	}


	public void setId(Long actionId) {
		this.actionId = actionId;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getElement() {
		return element;
	}


	public void setElement(String element) {
		this.element = element;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreation() {
		return creation;
	}


	public void setCreation(Date creation) {
		this.creation = creation;
	}


	public String getInvokedBy() {
		return invokedBy;
	}


	public void setInvokedBy(String invokedBy) {
		this.invokedBy = invokedBy;
	}

	@Lob
	public String getMoreAttributes() {
		return moreAttributes;
	}


	public void setMoreAttributes(String moreAttributes) {
		this.moreAttributes = moreAttributes;
	}


	@Override
	public String toString() {
		return "ActionEntity [actionId=" + actionId + ", type=" + type + ", element=" + element + ", creation="
				+ creation + ", invokedBy=" + invokedBy + ", moreAttributes=" + moreAttributes + "]";
	}


}
