package acs.dal;

import java.util.List;
//import java.util.Optional;

//import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
//import org.springframework.data.domain.Sort.Order;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
//import org.springframework.data.util.Streamable;

import acs.data.ElementEntity;

public interface ElementDao extends PagingAndSortingRepository<ElementEntity, Long>{
	
	public List<ElementEntity> findChildByOrigin(
			@Param("origin") ElementEntity origin, 
			Pageable pageable);
	
	public List<ElementEntity> findElementsByNameAndActive(
			@Param("name") String name,
			@Param("active") Boolean active,
			Pageable pageable); 
	
	public List<ElementEntity> findElementsByName(
			@Param("name") String name,
			Pageable pageable);
	
	public List<ElementEntity> findElementsByActive(
			@Param("active") Boolean active,
			Pageable pageable); 
	
	public List<ElementEntity> findElementsByType(
			@Param("type") String type,
			Pageable pageable); 
	
	public List<ElementEntity> findElementsByTypeAndActive(
			@Param("type") String type,
			@Param("active") Boolean active,
			Pageable pageable); 
	
	public List<ElementEntity> findElementsByElementID(
			@Param("elementID") Long elementID,
			Pageable pageable);

			}
