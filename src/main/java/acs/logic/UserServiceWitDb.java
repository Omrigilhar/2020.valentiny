package acs.logic;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import acs.boundary.UserBoundary;
import acs.boundary.UserRoleEnum;
import acs.dal.UserDao;
import acs.data.EntityConverter;
import acs.data.UserEntity;


@Service
public class UserServiceWitDb implements UserService {
	private UserDao userDao;
	private EntityConverter entityConverter;
	
	public UserServiceWitDb() {
	}
	
	@Autowired
	public UserServiceWitDb(UserDao userDao) {
		System.err.println("spring initialized me");
		this.userDao = userDao;
	}
	
	@Autowired
	public void setEntityConverter(EntityConverter entityConverter) {
		this.entityConverter = entityConverter;
	}
	
	@Override
	@Transactional//(readOnly = false)
	public UserBoundary createUser(UserBoundary user) {
	
		UserEntity entity = this.entityConverter.toEntity(user);
		
		if(user.getEmail() == null || user.getEmail() == "") {
			throw new NoEmailException("User email is empty.");
		}
		
		if(!EmailValidator.isValid(user.getEmail())){
			throw new EmailNotValidException("User email is not valid");
		}
		
		entity = this.userDao.save(entity);
		
		return this.entityConverter.convertFromEntity(entity);
	}

	@Override
	@Transactional(readOnly = true)
	public UserBoundary login(String userEmail) {
		// TODO Auto-generated method stub
		return getUserByEmail(userEmail);
	}

	@Override
	@Transactional //(readOnly = false)
	public UserBoundary updateUser(String userEmail, UserBoundary update) {
		UserBoundary existing = this.getUserByEmail(userEmail);
		boolean dirty = false;
		
		if (!update.getEmail().equals(existing.getEmail())) {
			throw new AttemptToChangeRestrictedAttribute("Original email cannot be changed, original email: " + existing.getEmail());
		}

		if (update.getRole() != null) {
			existing.setRole(update.getRole());
			dirty = true;
		}

		if (update.getUsername() != null || update.getUsername() != "") {
			existing.setUsername(update.getUsername());
			dirty = true;
		}

		if (update.getAvatar() != null) {
			existing.setAvatar(update.getAvatar());
			dirty = true;
		}
		
		if (update.getLocation() != null) {
			existing.setLocation(update.getLocation());
			dirty = true;
		}

		if (dirty) {
			this.userDao.save(this.entityConverter.toEntity(existing));
		}

		return existing;
	}

	@Override
	@Transactional(readOnly = true)
	public Collection<UserBoundary> getAllUsers(String adminEmail, int size, int page) {
		UserBoundary user = getUserByEmail(adminEmail);
		if(user.getRole() == UserRoleEnum.ADMIN) {
			return this.userDao.findAll(PageRequest.of(page, size, Direction.ASC, "email"))
									.getContent()
									.stream()
									.map(this.entityConverter::convertFromEntity)
									.collect(Collectors.toList());
		}else {
			throw new EmailNotValidException("Attemp to use admin action without admin premission.");
		}
	}

	@Override
	@Transactional//(readOnly = false)
	public void deleteAllUsers(String adminEmail) {
		this.userDao.deleteAll();

	}
	
	private UserBoundary getUserByEmail(String userEmail) {
		Optional<UserEntity> entity = this.userDao.findById(userEmail);
		
		if (entity.isPresent()) {
			return this.entityConverter.convertFromEntity(entity.get());
		}else {
			throw new NotFoundException("Could not find user for email: " + userEmail);
		}
	}
	
	public Boolean isEmailHavePermission(String email, UserRoleEnum userEnum) {
		UserBoundary user = this.getUserByEmail(email);
		if(user.getRole() == userEnum) {
			return true;
		}else {
			return false;
		}
	}
	
}
