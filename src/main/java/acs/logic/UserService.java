package acs.logic;

import java.util.Collection;

import acs.boundary.UserBoundary;

public interface UserService {
	
	public UserBoundary createUser(UserBoundary user);
	
	public UserBoundary login(String userEmail);
	
	public UserBoundary updateUser(String userEmail, UserBoundary update);
	
	public Collection<UserBoundary> getAllUsers(String adminEmail, int size, int page);
	
	public void deleteAllUsers(String adminEmail);
	
}
