package acs.logic;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import acs.boundary.ElementBoundary;
import acs.data.ElementEntity;
import acs.data.EntityConverter;


public class ElementServiceMockup implements ElementService{
	private Map<Long, ElementEntity> database; 
	private EntityConverter entityConverter;
	private AtomicLong nextId;
	
	public ElementServiceMockup() {
	}
	
	@Autowired
	public void setEntityConverter(EntityConverter entityConverter) {
		this.entityConverter = entityConverter;
	}

	@PostConstruct
	public void init() {
		// create thread safe list
		this.database = Collections.synchronizedMap(new TreeMap<>());
		this.nextId = new AtomicLong(0L);
	}
	
	@Override
	public ElementBoundary create(String managerEmail, ElementBoundary element) {
		//TODO: Need to combine and implement email check with other services
		if(element.getCreatedBy().isEmpty()) {
			throw new EmptyRequestException("Missing creator's name.");
		}
		else if(element.getLocation() == null) {
			throw new EmptyRequestException("Missing location.");
		}
		
		Long newId = nextId.incrementAndGet();
		ElementEntity entity = this.entityConverter.toEntity(element);
		
		entity.setCreation(new Date());
		entity.setElementID(newId);
		
		this.database.put(newId, entity);
		
		return this.entityConverter.convertFromEntity(entity);
	}

	@Override
	public ElementBoundary update(String managerEmail, String elementId, ElementBoundary update) {
		if(update.getCreatedBy().isEmpty()) {
			throw new EmptyRequestException("Missing creator's name.");
		}
		else if(update.getLocation() == null) {
			throw new EmptyRequestException("Missing location.");
		}
		
		ElementBoundary existing = this.getElementByID(elementId);
		boolean dirty = false;
	
		if(!update.getElementID().equals(existing.getElementID())) {
			throw new AttemptToChangeRestrictedAttribute("Attempt to change element ID attribute");
		}
		
		if(!update.getCreatedBy().equals(existing.getCreatedBy())) {
			throw new AttemptToChangeRestrictedAttribute("Attempt to change created by attribute");
		}
		
		if(!update.getCreation().equals(existing.getCreation())) {
			throw new AttemptToChangeRestrictedAttribute("Attempt to change date of creation attribute");
		}
		
		if (update.getName() != null || update.getName() != "") {
			existing.setName(update.getName());
			dirty = true;
		}

		if (update.isActive() != null) {
			existing.setActive(update.isActive());
			dirty = true;
		}

		if (update.getLocation() != null) {
			existing.setLocation(update.getLocation());
			dirty = true;
		}
		
		if (update.getMoreAttributes() != null) {
			existing.setMoreAttributes(update.getMoreAttributes());
			dirty = true;
		}

		if (dirty) {
			this.database.put(this.entityConverter.toEntityId(elementId),this.entityConverter.toEntity(existing));
		}
		
		return existing;
	}

	@Override
	public List<ElementBoundary> getAll(String userEmail) {
		//TODO: Need to combine and implement email check with other services
		return this.database.values().stream().map(entity->this.entityConverter.convertFromEntity(entity)).collect(Collectors.toList());
	}

	@Override
	public ElementBoundary getSpecificElement(String userEmail, String elementId) {
		//TODO: Need to combine and implement email check with other services
		return getElementByID(elementId);
	}

	@Override
	public void deleteAllElements(String adminEmail) {
		//TODO: Need to combine and implement email check with other services
		this.database.clear();	
	}

	private ElementBoundary getElementByID(String elementId) {
		
		ElementEntity entity = this.database.get(this.entityConverter.toEntityId(elementId));
		
		if (entity != null) {
			return this.entityConverter.convertFromEntity(entity);
		}else {
			throw new ElementNotFoundExcetpion("Could not find element for ID: " + elementId);
		}
	}
}
