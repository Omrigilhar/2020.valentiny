package acs.logic;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;

import acs.boundary.ActionBoundary;
import acs.data.ActionEntity;
import acs.data.EntityConverter;

//@Service
public class ActionServiceMockup implements ActionService {
	private Map<Long, ActionEntity> database;
	private EntityConverter entityConverter;
	private AtomicLong nextId;
	
	public ActionServiceMockup() {
	}
	
	@Autowired
	public void setEntityConverter(EntityConverter entityConverter) {
		this.entityConverter = entityConverter;	
	}
	
	@PostConstruct
	public void init() {
		// create thread safe list
		this.database = Collections.synchronizedMap(new TreeMap<>());
		this.nextId = new AtomicLong(0L);
	}
	
	@Override
	public Object invokeAction(ActionBoundary action) {
		Long newId = nextId.incrementAndGet();
		ActionEntity entity = this.entityConverter.toEntity(action);
		
		if (action.getType() == "") {
			throw new EmptyRequestException("No type included");
		}
		
		if (action.getInvokedBy().isEmpty()) {
			throw new EmptyRequestException("No invoked by included");
		}
		
		entity.setCreation(new Date());
		entity.setId(newId);
		
		this.database.put(newId, entity);
		
		return this.entityConverter.convertFromEntity(entity);
	}

	@Override
	public Collection<ActionBoundary> getAllActions(String adminEmail, int size, int page) {
		return this.database.values().stream().map(entity -> this.entityConverter.convertFromEntity(entity)).collect(Collectors.toList());
	}

	@Override
	public void deleteAllActions(String adminEmail) {
		this.database.clear();
	}

}
