package acs.logic;


import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import acs.boundary.ActionBoundary;
import acs.boundary.UserRoleEnum;
import acs.dal.ActionDao;
import acs.dal.LastIdValue;
import acs.dal.LastValueDao;
import acs.data.ActionEntity;
import acs.data.EntityConverter;


@Service
public class ActionServiceWihDb implements ActionService{
	private ActionDao actionDao;
	private UserServiceWitDb userService;
	private EntityConverter entityConverter;
	private LastValueDao lastValueDao;
	private ElementService elementService;
	
	@Autowired
	public ActionServiceWihDb(ActionDao actionDao, LastValueDao lastValueDao, UserServiceWitDb userService, ElementService elementService) {
		this.actionDao = actionDao;
		this.userService = userService;
		this.lastValueDao = lastValueDao;
		this.elementService = elementService;
	}
	
	@Autowired
	public void setEntityConverter(EntityConverter entityConverter) {
		this.entityConverter = entityConverter;
	}
	
	@Override
	@Transactional
	public Object invokeAction(ActionBoundary action) {
		if(action.getType().isEmpty()) {
			throw new EmptyRequestException("No type included");
		}
		else if(action.getInvokedBy() == null) {
			throw new EmptyRequestException("No invoked by included");
		}
		
		// Get the email from the createdBy attribute in the input ActionBoundary
		Map<String, String> userEmailMap = action.getInvokedBy();
		String userEmail = userEmailMap.get("email");
		
		Map<String, String> elementMap = action.getElement();
		String elementId = elementMap.get("ElementID");
		
		if(!elementService.getSpecificElement(userEmail, elementId).isActive()) {
			throw new NotFoundException("Element " + elementId + " was not found.");
		}
		
		if(!this.userService.isEmailHavePermission(userEmail, UserRoleEnum.PLAYER)) {
			throw new EmailNotValidException("Tried to invoke an action without an player permission");
		}
		LastIdValue idValue = this.lastValueDao.save(new LastIdValue());
		
		ActionEntity entity = this.entityConverter.toEntity(action);
		
		entity.setId(idValue.getLastId());// use newly generated id
		entity.setCreation(new Date());
		
		this.lastValueDao.delete(idValue);// cleanup redundant data
		
		entity = this.actionDao.save(entity); // UPSERT:  SELECT  -> UPDATE / INSERT
		
		return this.entityConverter.convertFromEntity(entity);	
	}

	@Override
	@Transactional(readOnly = true)
	public Collection<ActionBoundary> getAllActions(String adminEmail, int size, int page) {
		if(!EmailValidator.isValid(adminEmail) || !userService.isEmailHavePermission(adminEmail, UserRoleEnum.ADMIN)) {
			throw new EmailNotValidException("Email is not valid");
		}
		System.out.println("Im inside after checking the user mail");
		return this.actionDao.findAll(PageRequest.of(page, size, Direction.ASC, "id"))
								.getContent()
								.stream()
								.map(this.entityConverter::convertFromEntity)
								.collect(Collectors.toList());
	}

	@Override
	@Transactional
	public void deleteAllActions(String adminEmail) {
		if(!EmailValidator.isValid(adminEmail)) {
			throw new EmailNotValidException("Email is not valid");
		}
		this.actionDao.deleteAll();
		
	}

}
