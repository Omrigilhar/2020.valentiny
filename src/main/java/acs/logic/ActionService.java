package acs.logic;

import java.util.Collection;

import acs.boundary.ActionBoundary;

public interface ActionService {
	
public Object invokeAction(ActionBoundary action);

public Collection<ActionBoundary> getAllActions(String adminEmail, int size, int page);

public void deleteAllActions(String adminEmail);

}
