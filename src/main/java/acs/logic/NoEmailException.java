package acs.logic;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NoEmailException extends RuntimeException {
	private static final long serialVersionUID = 6917903622153393225L;
	
	public NoEmailException() {
		super();
	}

	public NoEmailException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoEmailException(String message) {
		super(message);
	}

	public NoEmailException(Throwable cause) {
		super(cause);
	}


}
