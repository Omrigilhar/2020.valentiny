package acs.logic;

import java.util.Collection;

import acs.boundary.ElementBoundary;

public interface EnhancedElementService extends ElementService{
	
	public void bindChildToElement(String managerEmail, String parentElementId, ElementBoundary childBoundary);
	
	public Collection<ElementBoundary> getAllElements(String userEmail, int size, int page);
	
	public Collection<ElementBoundary> getChildren(String userEmail, String parentElementId, int size, int page);

	public Collection<ElementBoundary> getOrigins(String userEmail, String childElementId, int size, int page);
	
	public Collection<ElementBoundary> getElementByName(String userEmail, String nameToSearchBy, int size, int page);

	public Collection<ElementBoundary> getElementByType(String userEmail, String typeToSearchBy, int size, int page);
	
}
