package acs.logic;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class AttemptToChangeRestrictedAttribute extends RuntimeException {
	private static final long serialVersionUID = -9202764452155601754L;
	
	public AttemptToChangeRestrictedAttribute() {
		super();
	}

	public AttemptToChangeRestrictedAttribute(String message, Throwable cause) {
		super(message, cause);
	}

	public AttemptToChangeRestrictedAttribute(String message) {
		super(message);
	}

	public AttemptToChangeRestrictedAttribute(Throwable cause) {
		super(cause);
	}
}
