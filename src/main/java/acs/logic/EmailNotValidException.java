package acs.logic;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EmailNotValidException extends RuntimeException {
	private static final long serialVersionUID = 3905570994280896152L;
	
	public EmailNotValidException() {
		super();
	}

	public EmailNotValidException(String message, Throwable cause) {
		super(message, cause);
	}

	public EmailNotValidException(String message) {
		super(message);
	}

	public EmailNotValidException(Throwable cause) {
		super(cause);
	}
}
