package acs.logic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;

import acs.boundary.ElementBoundary;
import acs.boundary.UserRoleEnum;
import acs.dal.ElementDao;
import acs.dal.LastIdValue;
import acs.dal.LastValueDao;
import acs.data.ElementEntity;
import acs.data.EntityConverter;

@Service
public class ElementServiceWitDb implements EnhancedElementService{
	private ElementDao elementDao;
	private EntityConverter entityConverter;
	private LastValueDao lastValueDao;
	private UserServiceWitDb userService;
	
	@Autowired
	public ElementServiceWitDb(ElementDao elementDao, LastValueDao lastValueDao, UserServiceWitDb userService) {
		this.elementDao = elementDao;
		this.lastValueDao = lastValueDao;
		this.userService = userService;
	}
	
	@Autowired
	public void setEntityConverter(EntityConverter entityConverter) {
		this.entityConverter = entityConverter;
	}
	
	@Override
	@Transactional
	public ElementBoundary create(String managerEmail, ElementBoundary newElement) {
		if(newElement.getLocation() == null) {
			throw new EmptyRequestException("Missing location.");
		}
		if(!EmailValidator.isValid(managerEmail) || !userService.isEmailHavePermission(managerEmail, UserRoleEnum.MANAGER)) {
			throw new EmailNotValidException("Email is not valid");
		}
		
		Map<String, String> createdBy =new HashMap<String, String>();
		createdBy.put("ManagerEmail", managerEmail);
		
		newElement.setCreatedBy(createdBy);
		
		LastIdValue idValue = this.lastValueDao.save(new LastIdValue());
		
		ElementEntity entity = this.entityConverter.toEntity(newElement);
		
		entity.setCreation(new Date());
		entity.setElementID(idValue.getLastId());// use newly generated id
		
		this.lastValueDao.delete(idValue);// cleanup redundant data
		
		entity = this.elementDao.save(entity); // UPSERT:  SELECT  -> UPDATE / INSERT
		
		return this.entityConverter.convertFromEntity(entity);
	}

	@Override
	@Transactional
	public ElementBoundary update(String managerEmail, String elementId, ElementBoundary update) {
		if(!EmailValidator.isValid(managerEmail) || !userService.isEmailHavePermission(managerEmail, UserRoleEnum.MANAGER)) {
			throw new EmailNotValidException("Email is not valid");
		}
		if(update.getLocation() == null) {
			throw new EmptyRequestException("Missing location.");
		}
		
		ElementBoundary existing = this.getElementById(elementId);
		boolean dirty = false;
		
		if(update.getElementID() != null && !update.getElementID().equals(elementId)) {
			throw new AttemptToChangeRestrictedAttribute("Attempt to change element ID attribute");
		}
		
		if(update.getCreation() != null && !update.getCreation().equals(existing.getCreation())) {
			throw new AttemptToChangeRestrictedAttribute("Attempt to change date of creation attribute");
		}

		if (update.getName() != null && update.getName() != "") {
			existing.setName(update.getName());
			dirty = true;
		}
		
		if (update.isActive() != null) {
			existing.setActive(update.isActive());
			dirty = true;
		}

		if (update.getType() != null) {
			existing.setType(update.getType());
			dirty = true;
		}

		if (update.getMoreAttributes() != null) {
			existing.setMoreAttributes(update.getMoreAttributes());
			dirty = true;
		}

		if (update.getLocation() != null) {
			existing.setLocation(update.getLocation());
			dirty = true;
		}
		
		if (dirty) {
			this.elementDao.save(this.entityConverter.toEntity(existing));
		}
		
		return existing;
	}

	@Override
	@Transactional(readOnly = true)
	public List<ElementBoundary> getAll(String userEmail) {
		if(!EmailValidator.isValid(userEmail) || !userService.isEmailHavePermission(userEmail, UserRoleEnum.PLAYER)) {
			throw new EmailNotValidException("Email is not valid");
		}
		List<ElementBoundary> rv = new ArrayList<>(); 
		Iterable<ElementEntity> content = this.elementDao.findAll();
		for (ElementEntity elem : content) {
			rv.add(this.entityConverter.convertFromEntity(elem));
		}
		return rv;
	}
	
	@Override
 	@Transactional(readOnly = true)
	public Collection<ElementBoundary> getAllElements(String userEmail, int size, int page) {
		if(!EmailValidator.isValid(userEmail)) {
			throw new EmailNotValidException("Email is not valid");
		}
		if(userService.isEmailHavePermission(userEmail, UserRoleEnum.PLAYER)) {
			return this.elementDao.findElementsByActive(true,
					PageRequest.of(page, size, Direction.ASC, "creation", "elementID"))
					.stream()
					.map(this.entityConverter::convertFromEntity)
					.collect(Collectors.toList());
		}else if(userService.isEmailHavePermission(userEmail, UserRoleEnum.MANAGER)) {
			return this.elementDao.findAll(PageRequest.of(page, size, Direction.ASC, "creation", "elementID"))
					.getContent()
					.stream()
					.map(this.entityConverter::convertFromEntity)
					.collect(Collectors.toList());
		}
		return null;
		
	}

	@Override
	@Transactional(readOnly = true)
	public ElementBoundary getSpecificElement(String userEmail, String elementId) {
		if(!EmailValidator.isValid(userEmail) || !userService.isEmailHavePermission(userEmail, UserRoleEnum.PLAYER)) {
			throw new EmailNotValidException("Email is not valid");
		}
		Optional<ElementEntity> entity = this.elementDao.findById(Long.parseLong(elementId));
		
		if (entity.isPresent()) {
			return this.entityConverter.convertFromEntity(entity.get());
		}else {
			throw new ElementNotFoundExcetpion ("could not find element for id: " + elementId);
		}
	}

	@Override
	@Transactional
	public void deleteAllElements(String managerEmail) {
		if(!EmailValidator.isValid(managerEmail) || !userService.isEmailHavePermission(managerEmail, UserRoleEnum.MANAGER)) {
			throw new EmailNotValidException("Email is not valid");
		}
		this.elementDao.deleteAll();
	}

	@Transactional(readOnly = true)
	public ElementBoundary getElementById(String elementId) {
		
		// SELECT * FROM MESSAGES WHERE ID=? 
		Optional<ElementEntity> entity = this.elementDao.findById(Long.parseLong(elementId));
		
		if (entity.isPresent()) {
			return this.entityConverter.convertFromEntity(entity.get());
		}else {
			throw new ElementNotFoundExcetpion("Could not find element for ID: " + elementId);
		}
	}

	@Override
	@Transactional
	public void bindChildToElement(String managerEmail, String parentElementId, ElementBoundary childBoundary) {
		if(!EmailValidator.isValid(managerEmail) || !userService.isEmailHavePermission(managerEmail, UserRoleEnum.MANAGER)) {
			throw new EmailNotValidException("Email is not valid");
		}
		if (childBoundary.getElementID() != null && childBoundary.getElementID().equals(parentElementId)) {
			throw new RuntimeException("element can't be a child of itself");
		}
		
		ElementEntity origin = this.elementDao.findById(this.entityConverter.toEntityId(parentElementId))
				.orElseThrow(()->new ElementNotFoundExcetpion ("could not find element for id: " + parentElementId));
		
		ElementEntity child = this.elementDao.findById(this.entityConverter.toEntityId(childBoundary.getElementID()))
			.orElseThrow(()->new ElementNotFoundExcetpion ("could not find element for id: " + childBoundary.getElementID()));
		
		origin.addChild(child);
		
		this.elementDao.save(origin);
	}

	@Override
	@Transactional(readOnly = true)
	public Collection<ElementBoundary> getChildren(String userEmail, String parentElementId, int size, int page) {
		if(!EmailValidator.isValid(userEmail)) {
			throw new EmailNotValidException("Email is not valid");
		}
		ElementEntity origin = this.elementDao
				.findById(this.entityConverter.toEntityId(parentElementId))
				.orElseThrow(()->new ElementNotFoundExcetpion ("could not find element for id: " + parentElementId));
		
		return this.elementDao.findChildByOrigin(origin,
				PageRequest.of(page, size, Direction.ASC, "creation", "elementID")) //Page<ElementEntity>
				.stream()  // Stream<ElementEntity>
				.map(this.entityConverter::convertFromEntity)// Stream<ElementBoundary>
				.collect(Collectors.toList());	// List<ElementBoundary>
		
		
	}

	@Override
	@Transactional(readOnly = true)
	public Collection<ElementBoundary> getOrigins(String userEmail, String childElementId, int size, int page) {
		if(!EmailValidator.isValid(userEmail)) {
			throw new EmailNotValidException("Email is not valid");
		}
		ElementEntity response = this.elementDao
				.findById(this.entityConverter.toEntityId(childElementId))
				.orElseThrow(()->new ElementNotFoundExcetpion ("could not find element for id: " + childElementId));
		
		return this.elementDao.findElementsByElementID(response.getOrigin().getElementID(),
				PageRequest.of(page, size, Direction.ASC, "creation", "elementID")) //Page<ElementEntity>
				.stream()  // Stream<ElementEntity>
				.map(this.entityConverter::convertFromEntity)// Stream<ElementBoundary>
				.collect(Collectors.toList());	// List<ElementBoundary>
	}
	
	@Override
	public Collection<ElementBoundary> getElementByName(String userEmail, String nameToSearchBy, int size, int page) {
		if(!EmailValidator.isValid(userEmail)) {
			throw new EmailNotValidException("Email is not valid");
		}
		List<ElementBoundary> elements = new ArrayList<>();
		if(userService.isEmailHavePermission(userEmail, UserRoleEnum.PLAYER)) {
			elements = this.elementDao.findElementsByNameAndActive(nameToSearchBy,true,
					  PageRequest.of(page, size, Direction.ASC, "name"))
					  .stream()
					  .map(this.entityConverter::convertFromEntity)
					  .collect(Collectors.toList());
		}else if(userService.isEmailHavePermission(userEmail, UserRoleEnum.MANAGER)) {
			elements = this.elementDao.findElementsByName(nameToSearchBy,
					  PageRequest.of(page, size, Direction.ASC, "name"))
					  .stream()
					  .map(this.entityConverter::convertFromEntity)
					  .collect(Collectors.toList());
		}
		if(!elements.isEmpty()) {
			return elements;
		}else {
			throw new NotFoundException("Element with the name " + nameToSearchBy + " was not found");
		}
	}

	@Override
	public Collection<ElementBoundary> getElementByType(String userEmail, String typeToSearchBy, int size, int page) {
		if(!EmailValidator.isValid(userEmail)) {
			throw new EmailNotValidException("Email is not valid");
		}
		List<ElementBoundary> elements = new ArrayList<>();
		if(userService.isEmailHavePermission(userEmail, UserRoleEnum.PLAYER)) {
			elements = this.elementDao.findElementsByTypeAndActive(typeToSearchBy,true,
					  PageRequest.of(page, size, Direction.ASC, "name"))
					  .stream()
					  .map(this.entityConverter::convertFromEntity)
					  .collect(Collectors.toList());
		}else if(userService.isEmailHavePermission(userEmail, UserRoleEnum.MANAGER)) {
			elements = this.elementDao.findElementsByType(typeToSearchBy,
					  PageRequest.of(page, size, Direction.ASC, "type"))
					  .stream()
					  .map(this.entityConverter::convertFromEntity)
					  .collect(Collectors.toList());
		}
		if(!elements.isEmpty()) {
			return elements;
		}else {
			throw new NotFoundException("Element with the type " + typeToSearchBy + " was not found");
		}
	}
}
