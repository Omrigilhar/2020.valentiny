package acs.logic;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ElementNotFoundExcetpion extends RuntimeException {
	private static final long serialVersionUID = -4968319808440543037L;

	public ElementNotFoundExcetpion() {
		super();
	}

	public ElementNotFoundExcetpion(String message, Throwable cause) {
		super(message, cause);
	}

	public ElementNotFoundExcetpion(String message) {
		super(message);
	}

	public ElementNotFoundExcetpion(Throwable cause) {
		super(cause);
	}
	

}
