package acs.logic;

import java.util.Collection;
import java.util.Collections;
//import java.util.List;
import java.util.Map;
import java.util.TreeMap;
//import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import acs.boundary.UserBoundary;
import acs.data.EntityConverter;
import acs.data.UserEntity;

//@Service
public class UserServiceMockup implements UserService {
	private Map<String, UserEntity> database;
	private EntityConverter entityConverter;
	
	public UserServiceMockup() {
	}
	
	@Autowired
	public void setEntityConverter(EntityConverter entityConverter) {
		this.entityConverter = entityConverter;
	}

	@PostConstruct
	public void init() {
		// create thread safe list
		this.database = Collections.synchronizedMap(new TreeMap<>());
	}
	
	
	/**
	 * Adds new user to the database. 
	 * The user argument must have a email field filled with an email.
	 * <p>
	 * This method returns user entity, if the user send a request
	 * without an email it will return an exception.
	 * This method convert the user boundary into a user entity and
	 * save it to the DB. 
	 *
	 * @param  user  an user details class containing all the user relevent details.
	 * @return  	 a user entity containing user details.
	 * @throws ConvertException when jackson tried to convert a Map to String and faild.
	 * @author Omri Gilhar
	 */
	@Override
	public UserBoundary createUser(UserBoundary user) {
		UserEntity entity = this.entityConverter.toEntity(user);
		
		if(user.getEmail() == null || user.getEmail() == "") {
			throw new NoEmailException("User email is empty.");
		}
		
		this.database.put(entity.getEmail(), entity);
		
		return this.entityConverter.convertFromEntity(entity);
	}

	/**
	 * Retrieve user details. 
	 * The userEmail argument must have regester to our database.
	 * <p>
	 * This method returns user entity, if the user send a request
	 * without an valid email it will return an exception.
	 *
	 * @param  userEmail  an user email.
	 * @return  	 	  a user entity containing user details.
	 * @throws UserNotFoundExcetpion When the email not found in the database.
	 * @throws ConvertException when jackson tried to convert a Map to String and faild.
	 * @author Omri Gilhar
	 */
	@Override
	public UserBoundary login(String userEmail) {
		return this.getUserByEmail(userEmail);
	}

	/**
	 * Update user details fields. 
	 * The userEmail argument must have regester to our database.
	 * The update argument cannot sent with a different email 
	 * than the original.
	 * <p>
	 * This method returns user entity after it updated the user details,
	 * if the user send a request without an valid email it will
	 *  return an exception.
	 * 
	 * @param  userEmail  an user email.
	 * @param  update     an user details class containing all the user relevent details.
	 * @return  	 	  a user entity containing user details.
	 * @throws ConvertException when jackson tried to convert a Map to String and faild.
	 * @throws AttemptToChangeRestrictedAttribute When user tries to change his email.
	 * @author Omri Gilhar
	 */
	@Override
	public UserBoundary updateUser(String userEmail, UserBoundary update) {
		UserBoundary existing = this.getUserByEmail(userEmail);
		boolean dirty = false;
		
		if (!update.getEmail().equals(existing.getEmail())) {
			throw new AttemptToChangeRestrictedAttribute("Original email cannot be changed, original email: " + existing.getEmail());
		}

		if (update.getRole() != null) {
			existing.setRole(update.getRole());
			dirty = true;
		}

		if (update.getUsername() != null || update.getUsername() != "") {
			existing.setUsername(update.getUsername());
			dirty = true;
		}

		if (update.getAvatar() != null) {
			existing.setAvatar(update.getAvatar());
			dirty = true;
		}
		
		if (update.getLocation() != null) {
			existing.setLocation(update.getLocation());
			dirty = true;
		}

		if (dirty) {
			this.database.put(update.getEmail(), this.entityConverter.toEntity(existing));
		}

		return existing;
	}
	
	/**
	 * **WIP - EVERYONE HAVE ADMIN PERMISSIONS**
	 * <p>
	 * Get all users in the database. 
	 * The adminEmail argument must have regester to our database,
	 * and have admin permission (See comment on top!).
	 * <p>
	 * This method returns List of all user in the database including admins,
	 * if the user send a request without an valid email it will
	 * return an exception.
	 * 
	 * @param  adminEmail  a valid admin email.
	 * @return  	 	   a list of user entity.
	 * @throws ConvertException when jackson tried to convert a Map to String and faild.
	 * @throws UserNotFoundExcetpion When the email not found in the database.
	 * @author Omri Gilhar
	 */
//	@Override
//	public List<UserBoundary> getAllUsers(String adminEmail) {
//		// To check if the email is exists in our database.
//		this.getUserByEmail(adminEmail);
//		return this.database.values().stream().map(entity -> this.entityConverter.convertFromEntity(entity)).collect(Collectors.toList());
//	}

	/**
	 * **WIP - EVERYONE HAVE ADMIN PERMISSIONS**
	 * <p>
	 * Delete all users in the database. 
	 * The adminEmail argument must have regester to our database,
	 * and have admin permission (See comment on top!).
	 * <p>
	 * This method delete every entry in the database including admins, users.
	 * if the user send a request without an valid email it will
	 * return an exception.
	 * 
	 * @param  adminEmail  a valid admin email.
	 * @throws UserNotFoundExcetpion When the email not found in the database.
	 * @author Omri Gilhar
	 */
	@Override
	public void deleteAllUsers(String adminEmail) {
		// To check if the email is exists in our database.
		this.getUserByEmail(adminEmail);
		this.database.clear();	
	}

	/**
	 * Get a user from the DB by their email. 
	 * The userEmail argument must have regester to our database.
	 * <p>
	 * This method returns user entiry from database including
	 * admins and users.
	 * if the user send a request without an valid email it will
	 * return an exception.
	 * 
	 * @param  adminEmail  a valid admin email.
	 * @return  	 	   user entity.
	 * @throws ConvertException when jackson tried to convert a Map to String and faild.
	 * @throws UserNotFoundExcetpion When the email not found in the database.
	 * @author Omri Gilhar
	 */
	private UserBoundary getUserByEmail(String userEmail) {
		UserEntity entity = this.database.get(userEmail);
		
		if (entity != null) {
			return this.entityConverter.convertFromEntity(entity);
		}else {
			throw new NotFoundException("Could not find user for email: " + userEmail);
		}
	}

	@Override
	public Collection<UserBoundary> getAllUsers(String adminEmail, int size, int page) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
