package acs.boundary;

import java.util.Date;
import java.util.Map;

public class ElementBoundary {
	private String elementID;
	private String type;
	private String name;
	private Boolean active;
	private Date creation;
	private Map<String, String> createdBy;
	private LocationBoundary location;
	private Map<String, String> moreAttributes;
	
	public ElementBoundary() {
	}

	public ElementBoundary(String type, String name, Boolean active, Date creation,
			 LocationBoundary location, Map<String, String> elementAttributes) {
		super();
		this.type = type;
		this.name = name;
		this.active = active;
		this.creation = creation;
		this.location = location;
		this.moreAttributes = elementAttributes;
	}
	
	public ElementBoundary(String elementID) {
		super();
		this.elementID = elementID;
		this.location = new LocationBoundary();
	}

	public String getElementID() {
		return elementID;
	}

	public void setElementID(String elementID) {
		this.elementID = elementID;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean isActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getCreation() {
		return creation;
	}

	public void setCreation(Date creation) {
		this.creation = creation;
	}

	public Map<String, String> getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Map<String, String> createdBy) {
		this.createdBy = createdBy;
	}


	public Map<String, String> getMoreAttributes() {
		return moreAttributes;
	}

	public void setMoreAttributes(Map<String, String> moreAttributes) {
		this.moreAttributes = moreAttributes;
	}

	public LocationBoundary getLocation() {
		return location;
	}

	public void setLocation(LocationBoundary location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "ElementBoundary [elementID=" + elementID + ", type=" + type + ", name=" + name + ", active=" + active
				+ ", creation=" + creation + ", createdBy=" + createdBy + ", location=" + location
				+ ", elementAttributes=" + moreAttributes + "]";
	}
	
	
	
	
}
