package acs.boundary;

public class LocationBoundary {
	private Double latitude;
	private Double longitude;
	
	public LocationBoundary() {
		
	}
	
	public LocationBoundary(Double latitude, Double longitude) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	@Override
	public String toString() {
		return "LocationBoundary [latitude=" + latitude + ", longitude=" + longitude + "]";
	}
	
}
