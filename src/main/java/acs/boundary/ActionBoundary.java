package acs.boundary;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ActionBoundary {
	private String actionId;
	private String type;
	private Map<String, String> element;//Contains elementId
	private Date creation;
	private Map<String, String> invokedBy;
	private Map<String, String> actionAttributes;//We will add relevant attributes as needed
	
	public ActionBoundary(){
	}
	
	public ActionBoundary(String actionId){
		this.actionId = actionId;
		this.type = new String("");
		this.element = new HashMap<String, String>();
		this.creation = new Date();
		this.invokedBy = new HashMap<String, String>();
		this.actionAttributes = new HashMap<String, String>();		
	}	
	
	public String getActionId() {
		return actionId;
	}
	public void setActionId(String actionId) {
		this.actionId = actionId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Map<String, String> getElement() {
		return element;
	}
	public void setElement(Map<String, String> element) {
		this.element = element;
	}
	public Date getCreation() {
		return creation;
	}
	public void setCreation(Date createdTimestamp) {
		this.creation = createdTimestamp;
	}
	public Map<String, String> getInvokedBy() {
		return invokedBy;
	}
	public void setInvokedBy(Map<String, String> invokedBy) {
		this.invokedBy = invokedBy;
	}
	public Map<String, String> getActionAttributes() {
		return actionAttributes;
	}
	public void setActionAttributes(Map<String, String> actionAttributes) {
		this.actionAttributes = actionAttributes;
	}
	
}
