package acs.boundary;

public class UserBoundary {
	private String email;
	private UserRoleEnum role;
	private String username;
	private String avatar;
	private LocationBoundary location;
	

	public UserBoundary() {
	}
	
	public UserBoundary(String email, UserRoleEnum role, String username, String avatar,LocationBoundary location) {
		this.email = email;
		this.role = role;
		this.username = username;
		this.avatar = avatar;
		this.location = location;
	}
	
	public UserBoundary(String email) {
		this.email = email;
		this.role = UserRoleEnum.PLAYER;
		this.username = "Demo User";
		this.avatar = "-_-";
		this.location = new LocationBoundary();
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UserRoleEnum getRole() {
		return role;
	}

	public void setRole(UserRoleEnum role) {
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public LocationBoundary getLocation() {
		return location;
	}

	public void setLocation(LocationBoundary location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "UserBoundary [email=" + email + ", role=" + role + ", username=" + username + ", avatar=" + avatar
				+ ", location=" + location + "]";
	}
}
