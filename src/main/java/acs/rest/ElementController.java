package acs.rest;
import java.util.Collection;
//import java.util.List;
//import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import acs.boundary.ElementBoundary;
import acs.logic.EnhancedElementService;

@RestController
public class ElementController {
	
	
	private EnhancedElementService elementService;
	
	@Autowired
	public ElementController(EnhancedElementService elementService) {
		super();
		this.elementService = elementService;
	}
	
	@RequestMapping(path = "/acs/elements/{userEmail}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public Collection<ElementBoundary> getAllElements (@PathVariable("userEmail") String userEmail, 
			@RequestParam(name = "size", required = false, defaultValue = "10") int size,
			@RequestParam(name = "page", required = false, defaultValue = "0") int page) {
		 return this.elementService.getAllElements(userEmail, size, page);
	}
	
	@RequestMapping(path = "/acs/elements/{userEmail}/{elementID}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ElementBoundary getElement(@PathVariable("userEmail") String userEmail,
							  @PathVariable("elementID") String elementID) {
		return this.elementService.getSpecificElement(userEmail, elementID);
	}
	
	@RequestMapping(path = "/acs/elements/{managerEmail}",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ElementBoundary createElement(@RequestBody ElementBoundary input, @PathVariable("managerEmail") String managerEmail) {
		return this.elementService.create(managerEmail, input);
	}
	
	@RequestMapping(path = "/acs/elements/{managerEmail}/{elementId}",
			method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public void updateElement(@RequestBody ElementBoundary input,
			   @PathVariable("managerEmail") String managerEmail, @PathVariable("elementId") String elementId) {
		this.elementService.update(managerEmail, elementId, input);
	}
	
	@RequestMapping(path = "/acs/elements/{managerEmail}/{parentElementId}/children",
			method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public void bindElement(@RequestBody ElementBoundary input,
			   				@PathVariable("managerEmail") String managerEmail, 
			   				@PathVariable("parentElementId") String parentElementId) {
		this.elementService.bindChildToElement(managerEmail, parentElementId, input);
	}
	
	@RequestMapping(path = "/acs/elements/{userEmail}/{parentElementId}/children",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public Collection<ElementBoundary> getAllChildrens(@PathVariable("userEmail") String userEmail,
							  					@PathVariable("parentElementId") String parentElementId,
							  					@RequestParam(name = "size", required = false, defaultValue = "10") int size,
							  					@RequestParam(name = "page", required = false, defaultValue = "0") int page) {
		return this.elementService.getChildren(userEmail, parentElementId, size, page);
	}
	
	@RequestMapping(path = "/acs/elements/{userEmail}/{childElementId}/parents",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public Collection<ElementBoundary> getAllParents(@PathVariable("userEmail") String userEmail,
							  					@PathVariable("childElementId") String childElementId,
							  					@RequestParam(name = "size", required = false, defaultValue = "10") int size,
							  					@RequestParam(name = "page", required = false, defaultValue = "0") int page) {
		return this.elementService.getOrigins(userEmail, childElementId, size, page);
	}
	
	@RequestMapping(path = "/acs/elements/{userEmail}/search/byName/{name}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public Collection<ElementBoundary> getElementsByName(@PathVariable("userEmail") String userEmail,
							  							 @PathVariable("name") String nameToSearchBy,
							  							 @RequestParam(name = "size", required = false, defaultValue = "10") int size,
									  					 @RequestParam(name = "page", required = false, defaultValue = "0") int page) {
		return this.elementService.getElementByName(userEmail, nameToSearchBy, size, page);
	}
	
	@RequestMapping(path = "/acs/elements/{userEmail}/search/byType/{type}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public Collection<ElementBoundary> getElementsByType(@PathVariable("userEmail") String userEmail,
							  							 @PathVariable("type") String typeToSearchBy,
							  							 @RequestParam(name = "size", required = false, defaultValue = "10") int size,
									  					 @RequestParam(name = "page", required = false, defaultValue = "0") int page) {
		return this.elementService.getElementByType(userEmail, typeToSearchBy, size, page);
	}
	
}
