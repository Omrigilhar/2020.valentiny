package acs.rest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import acs.boundary.UserBoundary;
import acs.logic.UserService;

@RestController
public class UserController {
	private UserService userService;
	
	@Autowired
	public UserController(UserService userService) {
		super();
		this.userService = userService;
	}
	
	
	@RequestMapping(path = "/acs/users/login/{userEmail}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	// Login valid user and retrieve user details
	public UserBoundary loginRetrieveDetails (@PathVariable("userEmail") String userEmail) {
		
		return this.userService.login(userEmail);
		
	}
	
	@RequestMapping(path = "/acs/users",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	// Login valid user and create new user
	public UserBoundary createNewUser (@RequestBody UserBoundary user) {
		
		return this.userService.createUser(user);
	}
	
	@RequestMapping(path = "/acs/users/{userEmail}",
			method = RequestMethod.PUT,		
			consumes = MediaType.APPLICATION_JSON_VALUE)
	// Login valid user and retrieve user details
	public void updateUserDetails (@RequestBody UserBoundary update,
								   @PathVariable("userEmail") String userEmail) {
		this.userService.updateUser(userEmail, update);
		
	}
}