package acs.rest;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import acs.boundary.ActionBoundary;
import acs.boundary.UserBoundary;
import acs.logic.ElementService;
import acs.logic.ActionService;
import acs.logic.UserService;

@RestController
public class AdminController {
	
private UserService userService;
private ElementService elementService;
private ActionService actionService;
	
	@Autowired
	public AdminController(UserService userService, ElementService elementService, ActionService actionService) {
		super();
		this.userService = userService;
		this.elementService = elementService;
		this.actionService = actionService;
	}

	
	@RequestMapping(path = "/acs/admin/users/{adminEmail}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	// Login valid user or admin and retrieve all students
	public Collection<UserBoundary> getAllUsers(@PathVariable("adminEmail") String adminEmail, 
			@RequestParam(name = "page", required = false, defaultValue = "0") int page, 
			@RequestParam(name = "size", required = false, defaultValue = "10") int size){
	    return this.userService.getAllUsers(adminEmail, size, page);
	  }
	
	@RequestMapping(path = "/acs/admin/actions/{adminEmail}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	// Login valid user and retrieve user details
	public Collection<ActionBoundary> getAllActions(
			@PathVariable("adminEmail") String adminEmail, 
			@RequestParam(name = "page", required = false, defaultValue = "0") int page, 
			@RequestParam(name = "size", required = false, defaultValue = "10") int size) {
		return this.actionService.getAllActions(adminEmail, size, page);
	  }
	
	@RequestMapping(path = "/acs/admin/users/{managerEmail}",
			method = RequestMethod.DELETE)
	// Login valid user and retrieve user details
	public void deleteAllUsers(@PathVariable("managerEmail") String managerEmail) {
		this.userService.deleteAllUsers(managerEmail);
	  }
	
	@RequestMapping(path = "/acs/admin/elements/{managerEmail}",
			method = RequestMethod.DELETE)
	// Login valid user and retrieve user details
	public void deleteAllElements(@PathVariable("managerEmail") String managerEmail) {
		 this.elementService.deleteAllElements(managerEmail);
	  }
	
	@RequestMapping(path = "/acs/admin/actions/{managerEmail}",
			method = RequestMethod.DELETE)
	// Login valid user and retrieve user details
	public void deleteAllActions(@PathVariable("managerEmail") String managerEmail) {
		this.actionService.deleteAllActions(managerEmail);
	  }
}
