package acs.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import javax.annotation.PostConstruct;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

import acs.boundary.LocationBoundary;
import acs.boundary.UserBoundary;
import acs.boundary.UserRoleEnum;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class UserControllerTest {
	private RestTemplate restTemplate;
	private String url;
	private int port;
	
	@LocalServerPort
	public void setPort(int port) {
		this.port = port;
	}
	
	@PostConstruct
	public void init() {
		this.restTemplate = new RestTemplate();
		this.url = "http://localhost:" + port + "/acs/users/";
	}
	
	@AfterEach
	public void teardown() {
		// cleanup test environment after each test
		this.restTemplate.delete("http://localhost:" + port + "/acs/admin/users/managerEmail@mail.com");
		
	}
	
	
	@Test
	public void testPostUserReturnsUserDetailsInResponse() throws Exception{
		// GIVEN server is up
		// do nothing

		// WHEN I POST /acs/users/ with a new user
		UserBoundary userToPost = new UserBoundary("test1@mail.afeka.ac.il", UserRoleEnum.PLAYER, "Test User", ":-)", new LocationBoundary());
		
		UserBoundary userFromServer = this.restTemplate.postForObject(this.url, userToPost, UserBoundary.class);
		
		// THEN the server responds with the new user details.
		assertThat(userFromServer).isEqualToComparingOnlyGivenFields(userToPost, "email", "role", "username", "avatar");
		
	}
	
	@Test
	public void testGetUserLoginDetails() throws Exception{
		// GIVEN server is up
		// AND there is a user with the same email "test1@mail.afeka.ac.il" in the database
		
		UserBoundary userToPost = new UserBoundary("test1@mail.afeka.ac.il", UserRoleEnum.PLAYER, "Test User", ":-)", new LocationBoundary());
		
		this.restTemplate.postForObject(this.url, userToPost, UserBoundary.class);
			
			
		// WHEN I GET /acs/users/login/
		UserBoundary loginDetails = this.restTemplate.getForObject(this.url + "login/test1@mail.afeka.ac.il", UserBoundary.class);
		
		// THEN the server responds with status 2xx
		// AND the response body is a user with at least an email.
		if (loginDetails.getEmail() == "" || loginDetails.getEmail() == null) {
			throw new Exception("Expected a user with email, got a user without any email.");
		}
		//
		assertEquals(loginDetails.getEmail(),"test1@mail.afeka.ac.il");
	}
	
	@Test
	public void testPutUpdateUserDetails() throws Exception{
		// GIVEN server is up
		// AND there at least one user in the database
		
		//WHEN I POST /acs/users/ Create a user in the database
		UserBoundary userToPost = new UserBoundary("test1@mail.afeka.ac.il", UserRoleEnum.PLAYER, "Test User", ":-)", new LocationBoundary());
		
		this.restTemplate.postForObject(this.url, userToPost, UserBoundary.class);
		
		UserBoundary userToPut = new UserBoundary("test1@mail.afeka.ac.il", UserRoleEnum.PLAYER, "Test User 11", ":-(", new LocationBoundary());
		
		//THEN I PUT /acs/users/  Update the current user in the database
		this.restTemplate.put(this.url + "test1@mail.afeka.ac.il", userToPut);
		
		//Get the user after update
		UserBoundary userAfterUpdate = this.restTemplate.getForObject(this.url + "login/test1@mail.afeka.ac.il", UserBoundary.class);
		
		// THEN Check that the new user from the database was updated with the new attributes
		
		assertThat(userAfterUpdate).isEqualToComparingOnlyGivenFields(userToPut, "username", "avatar");
	}
}
