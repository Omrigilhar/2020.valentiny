package acs.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.AbstractMap;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

import acs.boundary.ElementBoundary;
import acs.boundary.LocationBoundary;
import acs.boundary.UserBoundary;
import acs.boundary.UserRoleEnum;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ElementControllerTest {
	private RestTemplate restTemplate;
	private String url;
	private int port;
	private UserBoundary user;
	private UserBoundary manager;
	private UserBoundary admin;
	
	@LocalServerPort
	public void setPort(int port) {
		this.port = port;
		this.prepareTestEnvoronment();
	}
	
	@PostConstruct
	public void init() {
		this.restTemplate = new RestTemplate();
		this.url = "http://localhost:" + port + "/acs/elements";
	}
	
	@AfterEach
	public void teardown() {
		// cleanup test environment after each test
		this.restTemplate.delete("http://localhost:" + port + "/acs/admin/elements/managerEmail@mail.afeka.ac.il");
	}
	
	@Test
	public void testPostCreateAElementWithProperDetailsInTheDatabase() throws Exception{
		// GIVEN server is up
		// do nothing

		// WHEN I POST /acs/elements/{managerEmail} with a new element
		HashMap<String, String> createdBy = new HashMap<>();
		createdBy.put("MANAGER", "Omri");
		
		HashMap<String, String> moreAttributes = new HashMap<>();
		moreAttributes.put("Printer", "Exists");
		
		Date creationDate = new Date();
		
		ElementBoundary elementToPost
		= new ElementBoundary(
				"Hub", 
				"Afeka Library", 
				true, 
				creationDate,
				new LocationBoundary(31.955662323483433, 34.829224346788244), 
				moreAttributes);
		
		ElementBoundary elementFromServer = 
				this.restTemplate
				.postForObject(
						this.url + "/managerEmail@mail.afeka.ac.il",
						elementToPost,
						ElementBoundary.class);
		
		// THEN the server responds with the same element details, except for the timestamp and the id
		assertThat(elementFromServer)
		.isEqualToComparingOnlyGivenFields
		(elementToPost, "type", "name", "active", "moreAttributes");
	}

	@Test
	public void testGetSpecificElementFromDatabase() throws Exception{
		// GIVEN server is up
		// AND there is an element in the database with ID 1

		Map.Entry<String,String> moreAttributesEntry =
			    new AbstractMap.SimpleEntry<String, String>("Printer", "Exists");
		
		ElementBoundary newElement = this.AddElementToDB(
				"managerEmail@mail.afeka.ac.il",
				moreAttributesEntry,
				"Hub",
				"Afeka Library",
				true);

		// WHEN I GET /acs/elements/{userEmail}/{elementID}
		ElementBoundary elementDetails = 
				this.restTemplate.getForObject(
						this.url + "/userEmail@mail.afeka.ac.il/" + newElement.getElementID(),
						ElementBoundary.class);
		
		// THEN the server responds with status 2xx
		// AND the response body is an element.
		assertThat(newElement)
		.isEqualToComparingOnlyGivenFields
		(elementDetails, "type", "name", "active", "moreAttributes");
	}
	
	@Test
	public void testGetAllElementFromDatabase() throws Exception{
		// GIVEN server is up
		// AND there at least 2 elements in the database
		
		Map.Entry<String,String> moreAttributesEntry =
			    new AbstractMap.SimpleEntry<String, String>("Printer", "Exists");
		
		this.AddElementToDB(
				"managerEmail@mail.afeka.ac.il",
				moreAttributesEntry,
				"Hub",
				"Afeka Library",
				true);

		this.AddElementToDB(
				"managerEmail@mail.afeka.ac.il",
				moreAttributesEntry,
				"Hub2",
				"Afeka2 Library",
				true);
		
		// WHEN I GET /acs/elements/{userEmail}
		ElementBoundary[] elementsList = 
				this.restTemplate.getForObject(
						this.url + "/managerEmail@mail.afeka.ac.il",
						ElementBoundary[].class);
		
		// THEN the server responds with status 2xx
		// AND the response a list of elements.
		if(elementsList.length == 0) {
			throw new Exception("The element list was empty, expected at least 2 elements.");
		}
	}
	
	@Test
	public void testPutUpdateElement() throws Exception{
		// GIVEN server is up
		// AND there is at least one element in the database with the ID 1

		HashMap<String, String> moreAttributes = new HashMap<>();
		moreAttributes.put("Printer", "Exists");
		
		Map.Entry<String,String> moreAttributesEntry =
			    new AbstractMap.SimpleEntry<String, String>("Printer", "Exists");
		
		ElementBoundary newElement = this.AddElementToDB(
				"managerEmail@mail.afeka.ac.il",
				moreAttributesEntry,
				"Hub",
				"Afeka Library",
				true);
		
		ElementBoundary elementToPut = 
				new ElementBoundary(
						"Hub2",
						"afeka lib",
						false,
						null,
						new LocationBoundary(31.955662323483433, 34.829224346788244),
						moreAttributes);
		
		// WHEN I PUT /acs/elements/{managerEmail}/{elementId} with an mutated element
		this.restTemplate.put(this.url+ "/managerEmail@mail.afeka.ac.il/" + newElement.getElementID(), elementToPut);
		
		// THEN the server responds with status 2xx
	}
	
	@Test
	public void testPutBindElements() throws Exception{
		// GIVEN server is up
		// AND there is at least two elements in the database with the ID 1 and 2
		
		Map.Entry<String,String> moreAttributesEntry =
			    new AbstractMap.SimpleEntry<String, String>("Printer", "Exists");
		
		ElementBoundary parentElement = this.AddElementToDB(
				"managerEmail@mail.afeka.ac.il",
				moreAttributesEntry,
				"Hub",
				"Afeka Library",
				true);	
		ElementBoundary childElement = this.AddElementToDB(
				"managerEmail@mail.afeka.ac.il",
				moreAttributesEntry,
				"Hub2",
				"Afeka Library2",
				true);
		// WHEN I PUT /acs/elements/{managerEmail}/{parentElementId}/children
		//				with an elementID to bind as APPLICATION_JSON_VALUE
		
		ElementBoundary elementToBind = new ElementBoundary();
		elementToBind.setElementID(childElement.getElementID());
		
		this.restTemplate.put(
				this.url + "/managerEmail@mail.afeka.ac.il/" + parentElement.getElementID() + "/children",
				elementToBind);
		// THEN the server responds with status 2xx
		// AND bind element with ID 2 to element with ID 1 (child = 2, parent = 1)
		
		ElementBoundary[] childernList = 
				this.restTemplate.getForObject(
						this.url + "/managerEmail@mail.afeka.ac.il/" + parentElement.getElementID() + "/children",
						ElementBoundary[].class);
		
		ElementBoundary[] parentList = 
				this.restTemplate.getForObject(
						this.url + "/managerEmail@mail.afeka.ac.il/" + childElement.getElementID() + "/parents",
						ElementBoundary[].class);
		
		assertEquals(childernList[0].getElementID(), childElement.getElementID());
		assertEquals(parentList[0].getElementID(), parentElement.getElementID());
		
	}
	
	@Test
	public void testGetAllChildrensWithProperDetails() throws Exception{
		// GIVEN server is up
		// AND there is at least 2 elements binded to an parent element
		
		Map.Entry<String,String> moreAttributesEntry =
			    new AbstractMap.SimpleEntry<String, String>("Printer", "Exists");
		
		ElementBoundary parentElement = this.AddElementToDB(
				"managerEmail@mail.afeka.ac.il",
				moreAttributesEntry,
				"Hub",
				"Afeka Library",
				true);	
		ElementBoundary childElementNo1 = this.AddElementToDB(
				"managerEmail@mail.afeka.ac.il",
				moreAttributesEntry,
				"Hub2",
				"Afeka Library2",
				true);
		ElementBoundary childElementNo2 = this.AddElementToDB(
				"managerEmail@mail.afeka.ac.il",
				moreAttributesEntry,
				"Hub3",
				"Afeka Library3",
				true);
		
		ElementBoundary elementToBind2 = new ElementBoundary();
		elementToBind2.setElementID("2");
		ElementBoundary elementToBind3 = new ElementBoundary();
		elementToBind3.setElementID("3");
		
		this.restTemplate.put(
				this.url + "/managerEmail@mail.afeka.ac.il/" + parentElement.getElementID() + "/children",
				childElementNo1);
		this.restTemplate.put(
				this.url + "/managerEmail@mail.afeka.ac.il/" + parentElement.getElementID() + "/children",
				childElementNo2);
		
		// WHEN I GET /acs/elements/{userEmail}/{parentElementId}/children
		ElementBoundary[] childernList = 
				this.restTemplate.getForObject(
						this.url + "/managerEmail@mail.afeka.ac.il/" + parentElement.getElementID() + "/children",
						ElementBoundary[].class);
		
		// THEN the server responds with status 2xx
		// AND return a list of children containing the IDs: 2 and 3
		assertEquals(2, childernList.length);
	}
	
	@Test
	public void testGetAllParentsWithProperDetails() throws Exception{
		// GIVEN server is up
		// AND there is at least 1 element binded to an parent element
		
		Map.Entry<String,String> moreAttributesEntry =
			    new AbstractMap.SimpleEntry<String, String>("Printer", "Exists");
		
		ElementBoundary parentElement = this.AddElementToDB(
				"managerEmail@mail.afeka.ac.il",
				moreAttributesEntry,
				"Hub",
				"Afeka Library",
				true);	
		ElementBoundary childElement = this.AddElementToDB(
				"managerEmail@mail.afeka.ac.il",
				moreAttributesEntry,
				"Hub2",
				"Afeka Library2",
				true);
		
		ElementBoundary elementToBind2 = new ElementBoundary();
		elementToBind2.setElementID(childElement.getElementID());
		
		// Bind
		this.restTemplate.put(
				this.url + "/managerEmail@mail.afeka.ac.il/" + parentElement.getElementID() + "/children",
				elementToBind2);
		
		// WHEN I GET /acs/elements/{userEmail}/{parentElementId}/children
		ElementBoundary[] parentsList = 
				this.restTemplate.getForObject(
						this.url + "/managerEmail@mail.afeka.ac.il/"+ childElement.getElementID() + "/parents",
						ElementBoundary[].class);
		
		// THEN the server responds with status 2xx
		// AND return a list of children containing the IDs: 2 and 3
		assertEquals(parentElement.getElementID(), parentsList[0].getElementID());
		
	}
	
	private ElementBoundary AddElementToDB(
			String email,
			Map.Entry<String, String> moreAttributesEntry,
			String Type,
			String name,
			Boolean active) {
		
		HashMap<String, String> moreAttributes = new HashMap<>();
		moreAttributes.put(moreAttributesEntry.getKey(), moreAttributesEntry.getValue());
		
		ElementBoundary elementToInsert
		= new ElementBoundary(
				Type, 
				name, 
				active, 
				new Date(),
				new LocationBoundary(31.955662323483433, 34.829224346788244), 
				moreAttributes);
		
		ElementBoundary elementReturned = this.restTemplate
		.postForObject(
				this.url + "/" + email,
				elementToInsert,
				ElementBoundary.class);
		
		return elementReturned;
	}
	
	private void prepareTestEnvoronment() {
		// Create 3 different users to feed the database for later use.
		this.user = new UserBoundary("userEmail@mail.afeka.ac.il", UserRoleEnum.PLAYER, "userTaster", ":>", new LocationBoundary()) ;
		this.manager = new UserBoundary("managerEmail@mail.afeka.ac.il", UserRoleEnum.MANAGER, "managerTester", ":>", new LocationBoundary()) ;
		this.admin = new UserBoundary("adminEmail@mail.afeka.ac.il", UserRoleEnum.ADMIN, "adminTester", ":>", new LocationBoundary()) ;
		
		
		// Feed database with users
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.postForObject("http://localhost:" + port + "/acs/users",
				user,
				UserBoundary.class);
		restTemplate.postForObject("http://localhost:" + port + "/acs/users",
				manager,
				UserBoundary.class);
		restTemplate.postForObject("http://localhost:" + port + "/acs/users",
				admin,
				UserBoundary.class);
	}

}
