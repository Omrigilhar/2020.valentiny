package acs.rest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.AbstractMap;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

import acs.boundary.ActionBoundary;
import acs.boundary.ElementBoundary;
import acs.boundary.LocationBoundary;
import acs.boundary.UserBoundary;
import acs.boundary.UserRoleEnum;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ActionControllerTest {
	private RestTemplate restTemplate;
	private String url;
	private int port;
	private UserBoundary user;
	private UserBoundary manager;
	private UserBoundary admin;
	private ElementBoundary elementAction;
	
	@LocalServerPort
	public void setPort(int port) {
		this.port = port;
	}
	
	@PostConstruct
	public void init() {
		this.prepareTestEnvorinment();
		this.restTemplate = new RestTemplate();
		this.url = "http://localhost:" + port + "/acs/actions/";
	}
	
	@AfterEach
	public void teardown() {
		// cleanup test environment after each test
		this.restTemplate.delete("http://localhost:" + port + "/acs/admin/actions/" + this.admin.getEmail());
	}
	
	@Test
	public void invokeAnActionTest() throws Exception{
		
		HashMap<String, String> elements = new HashMap<>();
		elements.put("ElementID", this.elementAction.getElementID());
		
		ActionBoundary actionToPost = new ActionBoundary();
		actionToPost.setType("Delete");
		actionToPost.setElement(elements);
		actionToPost.setInvokedBy(new HashMap<String, String>());
		actionToPost.getInvokedBy().put("email", user.getEmail());
		
		ActionBoundary actionFromServer = this.restTemplate.postForObject(this.url, actionToPost, ActionBoundary.class);
		assertThat(actionFromServer).isEqualToComparingOnlyGivenFields(actionToPost, "type", "invokedBy");
		
	}
	
	private void prepareTestEnvorinment() {
		// Create 3 different users to feed the database for later use.
		this.user = new UserBoundary("userEmail@mail.afeka.ac.il", UserRoleEnum.PLAYER, "userTaster", ":>", new LocationBoundary());
		this.manager = new UserBoundary("managerEmail@mail.afeka.ac.il", UserRoleEnum.MANAGER, "managerTester", ":>", new LocationBoundary());
		this.admin = new UserBoundary("adminEmail@mail.afeka.ac.il", UserRoleEnum.ADMIN, "adminTester", ":>", new LocationBoundary());
		
		// Feed database with users
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.postForObject("http://localhost:" + port + "/acs/users",
				user,
				UserBoundary.class);
		restTemplate.postForObject("http://localhost:" + port + "/acs/users",
				manager,
				UserBoundary.class);
		restTemplate.postForObject("http://localhost:" + port + "/acs/users",
				admin,
				UserBoundary.class);

		//Create an element to use as an element in the action form.
		HashMap<String, String> moreAttributes = new HashMap<>();
		moreAttributes.put("Att", "Test");
		
		this.elementAction = new ElementBoundary(
				"Test", 
				"Test", 
				true, 
				new Date(),
				new LocationBoundary(0.000, 0.000), 
				moreAttributes);
		
		// Feed database with the element
		this.elementAction = new RestTemplate()
		.postForObject(
				"http://localhost:" + port + "/acs/elements/managerEmail@mail.afeka.ac.il",
				this.elementAction,
				ElementBoundary.class);		
	}
	
}
