package acs;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ApplicationTests {
	@Test
	public void testContext() {
		String srt = new String("This is a test for Bit Bucket Pipelines");
		assertEquals(srt, "This is a test for Bit Bucket Pipelines");
	}
}
